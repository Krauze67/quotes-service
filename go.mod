module gitlab.com/Krauze67/quotes-service

go 1.12

require (
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23
	github.com/go-kit/kit v0.9.0
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/lib/pq v1.2.0
	github.com/stretchr/testify v1.4.0
	gitlab.com/Krauze67/flib v0.0.0-20190605093728-b4d5557c138e // indirect
	gitlab.com/Krauze67/quotes-service/cmd/quotes v0.0.0-00010101000000-000000000000
	gitlab.com/Krauze67/quotes-service/cmd/quotes/collector v0.0.0-00010101000000-000000000000
	golang.org/x/text v0.3.2 // indirect
)

replace (
	gitlab.com/Krauze67/quotes-service/cmd/quotes => ./cmd/quotes
	gitlab.com/Krauze67/quotes-service/cmd/quotes/collector => ./cmd/quotes/collector
)
