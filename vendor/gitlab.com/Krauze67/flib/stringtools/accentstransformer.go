package stringtools

import (
	"fmt"
	"unicode/utf8"

	"golang.org/x/text/transform"
)

// AccentsTransform part:
type AccentsTransformer map[rune]string

func (a AccentsTransformer) Reset() {
}

func (a AccentsTransformer) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
	for nSrc < len(src) {
		// If we're at the edge, note this and return.
		if !atEOF && !utf8.FullRune(src[nSrc:]) {
			err = transform.ErrShortSrc
			return
		}
		r, width := utf8.DecodeRune(src[nSrc:])
		if r == utf8.RuneError && width == 1 {
			err = fmt.Errorf("Decoding error")
			return
		}
		if d, ok := a[r]; ok {
			if nDst+len(d) > len(dst) {
				err = transform.ErrShortDst
				return
			}
			copy(dst[nDst:], d)
			nSrc += width
			nDst += len(d)
			continue
		}

		if nDst+width > len(dst) {
			err = transform.ErrShortDst
			return
		}
		copy(dst[nDst:], src[nSrc:nSrc+width])
		nDst += width
		nSrc += width
	}
	return
}
