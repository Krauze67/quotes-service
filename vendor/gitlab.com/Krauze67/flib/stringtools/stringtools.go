package stringtools

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"strings"
	"unicode"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

const (
	EnLower       = "qwertyuiopasdfghjklzxcvbnm"
	EnUpper       = "QWERTYUIOPASDFGHJKLZXCVBNM"
	EnCount       = 26
	EnPunctuation = ".,;:-!?\"()"

	NumbersDec          = "0123456789"
	ArithmeticOperators = "+-*/="

	EnConsonantsLower = "qwrtpsdfghjklzxcvbnm"
	EnVowelsLower     = "eyuaio"
	EnConsonantsUpper = "QWRTPSDFGHJKLZXCVBNM"
	EnVowelsUpper     = "EYUAIO"

	Spaces = " \t\n\r"

	//cHtmlSpecSymbol_MaxReplacementLength = 9 //these are "&Epsilon;", "&Omicron;", "&Upsilon;"...
	//cJsonSpecSymbol_MaxReplacementLength = 5 //there is "&amp;"

)

var (
	HtmlSpecSymbol     = [...]string{"&nbsp;", "&pound;", "&euro;", "&para;", "&sect;", "&copy;", "&reg;", "&trade;", "&deg;", "&plusmn;", "&frac14;", "&frac12;", "&frac34;", "&times;", "&divide;", "&fnof;", "&Alpha;", "&Beta;", "&Gamma;", "&Delta;", "&Epsilon;", "&Zeta;", "&Eta;", "&Theta;", "&Iota;", "&Kappa;", "&Lambda;", "&Mu;", "&Nu;", "&Xi;", "&Omicron;", "&Pi;", "&Rho;", "&Sigma;", "&Tau;", "&Upsilon;", "&Phi;", "&Chi;", "&Psi;", "&Omega;", "&alpha;", "&beta;", "&gamma;", "&delta;", "&epsilon;", "&zeta;", "&eta;", "&theta;", "&iota;", "&kappa;", "&lambda;", "&mu;", "&nu;", "&xi;", "&omicron;", "&pi;", "&rho;", "&sigmaf;", "&sigma;", "&tau;", "&upsilon;", "&phi;", "&chi;", "&psi;", "&omega;", "&larr;", "&uarr;", "&rarr;", "&darr;", "&harr;", "&spades;", "&clubs;", "&hearts;", "&diams;", "&quot;", "&amp;", "&lt;", "&gt;", "&hellip;", "&prime;", "&Prime;", "&ndash;", "&mdash;", "&lsquo;", "&rsquo;", "&sbquo;", "&ldquo;", "&rdquo;", "&bdquo;", "&laquo;", "&raquo;"}
	HtmlSpecSymbolCode = [...]int64{160, 163, 8364, 182, 167, 169, 174, 8482, 176, 177, 188, 189, 190, 215, 247, 402, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 931, 932, 933, 934, 935, 936, 937, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 8592, 8593, 8594, 8595, 8596, 9824, 9827, 9829, 9830, 34, 38, 60, 62, 8230, 8242, 8243, 8211, 8212, 8216, 8217, 8218, 8220, 8221, 8222, 171, 187}
)

func GetHtmlSpecSymbols() []string {
	return HtmlSpecSymbol[0 : len(HtmlSpecSymbol)-1]
}

func GetHtmlSpecSymbol(n int64) string {
	return HtmlSpecSymbol[n]
}

func GetHtmlSpecSymbolCode(n int64) int64 {
	return HtmlSpecSymbolCode[n]
}

func GetSubstringPtr(src string, before string, after string) *string {
	emptyStr := ""
	if 0 == len(src) {
		if 0 == len(before) && 0 == len(after) {
			return &emptyStr
		}
		return nil
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return nil
		}
		startIdx += len(before)
	}

	if startIdx == len(src) {
		if 0 == len(after) {
			return &emptyStr
		}
		return nil
	}

	endIdx := len(src)
	if 0 < len(after) {
		if startIdx > 0 {
			endIdx = strings.Index(src[startIdx:], after)
		} else {
			endIdx = strings.Index(src, after)
		}

		if 0 > endIdx {
			return nil
		}
		//Correct original string Idx:
		endIdx += startIdx
	}

	retValStr := src[startIdx:endIdx]
	return &retValStr
}

func GetSubstring(src string, before string, after string) (string, error) {
	if 0 == len(src) {
		if 0 == len(before) && 0 == len(after) {
			return "", nil
		}
		return "", errors.New("empty source")
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(src, before)
		if 0 > startIdx {
			return "", fmt.Errorf("'before'[%s] not found", before)
		}
		startIdx += len(before)
	}

	if startIdx == len(src) {
		if 0 == len(after) {
			return "", nil
		}
		return "", fmt.Errorf("after[%s] could not be found - 'before' ends the string", after)
	}

	endIdx := len(src)
	if 0 < len(after) {
		if startIdx > 0 {
			endIdx = strings.Index(src[startIdx:], after)
		} else {
			endIdx = strings.Index(src, after)
		}

		if 0 > endIdx {
			return "", fmt.Errorf("after[%s] not found", after)
		}
		//Correct original string Idx:
		endIdx += startIdx
	}

	retValStr := src[startIdx:endIdx]
	return retValStr, nil
}

func IsLatinOnly(str string, nonLettersAllowedSet string) bool {
	for _, r := range []rune(str) {
		isSmallLatin := (r >= 'a' && r <= 'z')
		isBigLatin := (r >= 'A' && r <= 'Z')
		isOtherOk := (0 <= strings.IndexRune(nonLettersAllowedSet, r))
		if !(isSmallLatin || isBigLatin || isOtherOk) {
			return false
		}
	}
	return true
}

func GetLatinView(str string, nonLettersAllowedSet string) string {

	//1. remove diactric marks like "žůžo":
	t := transform.Chain(norm.NFD, transform.RemoveFunc(func(r rune) bool {
		return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
	}), norm.NFC)
	noDiactr, _, _ := transform.String(t, str)

	//2. map similar symbols:
	// This map is used by RemoveAccents function to convert non-accented characters.
	var transliterations = AccentsTransformer{'Æ': "E", 'Ð': "D", 'Ł': "L", 'Ø': "OE",
		'Þ': "Th", 'ß': "ss", 'æ': "e", 'ð': "d", 'ł': "l", 'ø': "oe", 'þ': "th", 'Œ': "OE",
		'œ': "oe",
	}
	b := transform.NewReader(bytes.NewBufferString(noDiactr), transliterations)
	scanner := bufio.NewScanner(b)
	if scanner.Scan() {
		if noDiactrNoAccents := scanner.Text(); IsLatinOnly(noDiactrNoAccents, nonLettersAllowedSet) {
			return noDiactrNoAccents
		}
	}

	return ""
}
