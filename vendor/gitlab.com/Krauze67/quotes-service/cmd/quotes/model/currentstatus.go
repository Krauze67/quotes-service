package model

//
// CurrentStatus holds summary information about instrument.
//
type CurrentStatus struct {
	Current  float64 `json:"current,omitempty"`
	TodayAvg float64 `json:"today_average,omitempty"`
	WeekAvg  float64 `json:"week_average,omitempty"`
	MonthAvg float64 `json:"month_average,omitempty"`
}
