package model

import "time"

//
// Quote info.
//
type Quote struct {
	InstrumentID   int64  `json:"-"`
	InstrumentName string `json:"name,omitempty"`
	QuoteValue
}

//
// Quote value info.
//
type QuoteValue struct {
	ExchangeRate float64   `json:"exchangerate,omitempty"`
	Time         time.Time `json:"time,omitempty"`
}
