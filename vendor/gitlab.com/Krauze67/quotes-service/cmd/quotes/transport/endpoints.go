package transport

import (
	"context"
	"time"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/errors"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Endpoints holds all Go kit endpoints for the service.
//
type Endpoints struct {
	GetStatus        endpoint.Endpoint
	GetHistoryValue  endpoint.Endpoint
	GetHistorySeries endpoint.Endpoint
}

//
// MakeEndpoints initializes all Go kit endpoints for the service.
//
func MakeEndpoints(s quotes.Service) Endpoints {
	return Endpoints{
		GetStatus:        makeGetStatusEndpoint(s),
		GetHistoryValue:  makeGetHistoryValueEndpoint(s),
		GetHistorySeries: makeGetHistorySeriesEndpoint(s),
	}
}

func makeGetStatusEndpoint(s quotes.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		instrument := ""
		if req, ok := request.(StatusRequest); ok {
			instrument = req.Instrument
		}
		status, err := s.GetStatus(ctx, instrument)
		if err != nil {
			return nil, err
		}

		return StatusResponse{
			Instrument:    instrument,
			Time:          time.Now().UTC(),
			CurrentStatus: status,
		}, nil
	}
}

func makeGetHistoryValueEndpoint(s quotes.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		if req, ok := request.(HistoryValueRequest); ok {
			quote, err := s.GetHistoryValue(ctx, req.Instrument, req.Time)
			if err != nil {
				return nil, err
			}

			return HistoryValueResponse{
				Quote: quote,
			}, nil
		}
		return nil, errors.ErrInternal
	}
}

func makeGetHistorySeriesEndpoint(s quotes.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		if req, ok := request.(HistorySeriesRequest); ok {
			quotes, err := s.GetHistorySeries(
				ctx,
				req.Instrument,
				req.AggregationType,
				req.PeriodBegin,
				req.PeriodEnd,
			)
			if err != nil {
				return nil, err
			}

			response := HistorySeriesResponse{
				Instrument:  req.Instrument,
				PeriodBegin: req.PeriodBegin,
				PeriodEnd:   req.PeriodEnd,
				Quotes:      make([]model.QuoteValue, len(quotes)),
			}
			copy(response.Quotes, quotes)

			return response, nil
		}
		return nil, errors.ErrInternal
	}
}
