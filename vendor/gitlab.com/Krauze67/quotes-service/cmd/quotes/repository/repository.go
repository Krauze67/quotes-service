package repository

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/config"
	quoteerrors "gitlab.com/Krauze67/quotes-service/cmd/quotes/errors"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Database table names.
//
const (
	//
	// DB details.
	//
	tableInstruments = "instruments"
	tableQuotes      = "quotes"

	//
	// log constants.
	//
	errorMsgKey = "err"
)

type repository struct {
	db     *sql.DB
	logger kitlog.Logger
}

//
// Repository contains a repository for all operations.
//
var Repository *repository

//
// InitRepository creates a repository instance.
//
func InitRepository(log kitlog.Logger) error {

	// Database.
	var (
		db  *sql.DB
		err error
	)

	// Connect to the database.
	if db, err = sql.Open(
		config.Parameters.GetDBDriver(),
		config.Parameters.DatabaseDSN,
	); err != nil {
		return err
	}

	Repository = &repository{
		db:     db,
		logger: kitlog.With(log, "repo", "sqlDB"),
	}
	return nil
}

//
// SaveQuotes saves quotes to the DB.
//
func (repo *repository) SaveQuotes(ctx context.Context, quotes []*model.Quote) error {
	logger := kitlog.With(repo.logger, "method", "SaveQuotes")
	level.Debug(logger).Log(
		"callflow", "SaveQuotes",
		"quotescount", len(quotes),
	)

	var err error
	for _, quote := range quotes {

		if quote.InstrumentID, err = repo.getInstrumentIDOrAddInstrument(quote.InstrumentName); err != nil {
			level.Error(logger).Log(errorMsgKey, err.Error())
			return quoteerrors.ErrInternal
		}

		if err = repo.addQuote(quote); err != nil {
			level.Error(logger).Log(errorMsgKey, err.Error())
			return quoteerrors.ErrInternal
		}
	}

	return nil
}

//
// getInstrumentIDOrAddInstrument Returns instrument ID or adds instrument and returns its ID.
//
func (repo *repository) getInstrumentIDOrAddInstrument(instrument string) (int64, error) {

	//TODO(DF): optimize by	implementation of instruments cache
	// a map of previously processed instruments to fill out instrument IDs
	// and save them all by single query.

	var id int64
	query := fmt.Sprintf(`
		SELECT id FROM %s WHERE name=$1`,
		tableInstruments)

	err := repo.db.QueryRow(query, instrument).Scan(&id)
	if err == sql.ErrNoRows {
		query = fmt.Sprintf(`
		INSERT INTO %s (name) VALUES ($1) ON CONFLICT DO NOTHING
		RETURNING id`,
			tableInstruments)

		err = repo.db.QueryRow(query, instrument).Scan(&id)
	}

	return id, err
}

//
// addQuote Adds quote value.
//
func (repo *repository) addQuote(quote *model.Quote) error {

	query := fmt.Sprintf(
		`INSERT INTO %s (instrument_id, got_at, exchange_rate) VALUES ($1, $2, $3)`,
		tableQuotes)

	_, err := repo.db.Exec(query, quote.InstrumentID, quote.Time, quote.ExchangeRate)
	return err
}

//
// GetStatus implements Repository method.
//
func (repo *repository) GetStatus(
	ctx context.Context,
	instrument string,
) (model.CurrentStatus, error) {
	logger := kitlog.With(repo.logger, "method", "GetStatus")
	level.Debug(logger).Log(
		"callflow", "GetStatus",
		"instrument", instrument,
	)

	query := fmt.Sprintf(`
	SELECT 
		(SELECT q.exchange_rate as current_rate 
		FROM %[2]s as q LEFT JOIN %[1]s as i ON q.instrument_id=i.id 
		WHERE i.name = $1
			ORDER BY q.got_at DESC LIMIT 1
		) as current_rate,

		(SELECT sum(q.exchange_rate)/count(q.exchange_rate) 
		FROM %[2]s as q LEFT JOIN %[1]s as i ON q.instrument_id=i.id 
		WHERE i.name = $1
			AND q.got_at >= date_trunc('day', now())
		) as avg_rate_d,

		(SELECT sum(q.exchange_rate)/count(q.exchange_rate) 
		FROM %[2]s as q LEFT JOIN %[1]s as i ON q.instrument_id=i.id 
		WHERE i.name = $1
			AND q.got_at >= date_trunc('week', now())
		) as avg_rate_w,

		(SELECT sum(q.exchange_rate)/count(q.exchange_rate)
		FROM %[2]s as q LEFT JOIN %[1]s as i ON q.instrument_id=i.id 
		WHERE i.name = $1
			AND q.got_at >= date_trunc('month', now())
		) as avg_rate_mon`,
		tableInstruments,
		tableQuotes,
	)
	var status model.CurrentStatus

	err := repo.db.QueryRow(query, instrument).
		Scan(
			&status.Current,
			&status.TodayAvg,
			&status.WeekAvg,
			&status.MonthAvg,
		)
	if err != nil {
		level.Error(logger).Log(
			"instrument", instrument,
			"err", err.Error(),
		)
	}
	return status, err
}

//
// GetStatus implements Repository method.
//
func (repo *repository) GetHistoryValue(
	ctx context.Context,
	instrument string,
	timePoint time.Time,
) (model.Quote, error) {
	logger := kitlog.With(repo.logger, "method", "GetHistoryValue")
	level.Debug(logger).Log(
		"callflow", "GetHistoryValue",
		"instrument", instrument,
		"time", timePoint,
	)

	query := fmt.Sprintf(`
	SELECT q.exchange_rate, q.got_at, to_timestamp($1) - q.got_at as difference
	FROM %[2]s as q LEFT JOIN %[1]s as i ON q.instrument_id=i.id
	WHERE i.name = $2
	ORDER BY abs(extract(epoch from to_timestamp($1) - q.got_at)) ASC
	LIMIT 1`,
		tableInstruments,
		tableQuotes,
	)

	var quote model.Quote
	var diff string
	err := repo.db.QueryRow(query, timePoint.Unix(), instrument).
		Scan(
			&quote.ExchangeRate,
			&quote.Time,
			&diff,
		)
	if err != nil {
		level.Error(logger).Log(
			"instrument", instrument,
			"err", err.Error(),
		)
		return model.Quote{}, err
	}

	quote.InstrumentName = instrument
	return quote, err
}

//
// GetStatus implements Repository method.
//
func (repo *repository) GetHistorySeries(
	ctx context.Context,
	instrument string,
	joinMinutes int64,
	begin time.Time,
	end time.Time,
) ([]model.QuoteValue, error) {
	logger := kitlog.With(repo.logger, "method", "GetHistoryValue")
	level.Debug(logger).Log(
		"callflow", "GetHistorySeries",
		"instrument", instrument,
		"joinMinutes", fmt.Sprintf("%v", joinMinutes),
		"begin", fmt.Sprintf("%v", begin),
		"end", fmt.Sprintf("%v", end),
	)

	query := fmt.Sprintf(`
	SELECT
		to_timestamp(floor((extract('epoch' from q.got_at) / $1 )) * $1) 
		AT TIME ZONE 'UTC' as join_interval,
		sum(q.exchange_rate)/count(q.exchange_rate) as rate
	FROM %[2]s as q LEFT JOIN %[1]s as i ON q.instrument_id=i.id
	WHERE i.name = $2
		AND q.got_at >= to_timestamp($3)
		AND q.got_at <= to_timestamp($4)
	GROUP BY join_interval`,
		tableInstruments,
		tableQuotes,
	)

	rows, err := repo.db.Query(query,
		joinMinutes*60, // in seconds
		instrument,
		begin.Unix(),
		end.Unix(),
	)
	if err != nil {
		level.Error(logger).Log(errorMsgKey, err.Error())
		return []model.QuoteValue{}, quoteerrors.ErrInternal
	}
	defer rows.Close()

	var quote model.QuoteValue
	quotes := make([]model.QuoteValue, 0)
	for rows.Next() {

		if err = rows.Scan(&quote.Time, &quote.ExchangeRate); err != nil {
			level.Error(logger).Log("err", err.Error())
			return []model.QuoteValue{}, quoteerrors.ErrInternal
		}
		quotes = append(quotes, quote)
	}

	return quotes, nil
}
