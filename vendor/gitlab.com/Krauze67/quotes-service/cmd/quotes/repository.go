package quotes

import (
	"context"
	"time"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Repository describes the persistence on model.
//
type Repository interface {
	SaveQuotes(ctx context.Context, quotes []*model.Quote) error

	GetStatus(ctx context.Context, instrument string) (model.CurrentStatus, error)
	GetHistoryValue(ctx context.Context, instrument string, time time.Time) (model.Quote, error)
	GetHistorySeries(
		ctx context.Context,
		instrument string,
		joinMinutes int64,
		begin time.Time,
		end time.Time,
	) ([]model.QuoteValue, error)
}
