package log

import (
	"os"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/config"
)

//
// Logger
//
var Logger kitlog.Logger

//
// InitLogger is required to be called to create and initialize a Logger instance
//
func InitLogger() {

	Logger = kitlog.NewLogfmtLogger(os.Stderr)
	Logger = kitlog.NewSyncLogger(Logger)
	Logger = level.NewFilter(Logger, getLogLevelOption(config.Parameters.LogLevel))
	Logger = kitlog.With(Logger,
		"svc", config.ServiceName,
		"ts", kitlog.DefaultTimestampUTC,
		"caller", kitlog.DefaultCaller,
	)
}

func getLogLevelOption(lvl string) level.Option {
	switch lvl {
	case "DEBUG":
		{
			return level.AllowDebug()
		}
	case "INFO":
		{
			return level.AllowInfo()
		}
	case "WARNING":
		{
			return level.AllowWarn()
		}
	case "ERROR":
		{
			return level.AllowError()
		}
	}
	return level.AllowWarn()
}
