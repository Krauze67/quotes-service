package forexinvesting

import (
	"context"
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/Krauze67/flib/stringtools"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Forexinvesting loads quotes from https://Forex-investing.com
//
type Forexinvesting struct {
	logger kitlog.Logger
}

//
// New creates a new instance with logger initialized
//
func New(logger kitlog.Logger) *Forexinvesting {
	return &Forexinvesting{
		logger: logger,
	}
}

const (
	httpTimeoutLoad    = time.Duration(5) * time.Second
	httpTimeoutConnect = time.Duration(2) * time.Second

	// https://www.investing.com/currencies/single-currency-crosses
	addressMainPage = "https://www.investing.com/currencies/single-currency-crosses"
)

//
// LoadQuotes implements qoutes.Loader interface
//
func (l *Forexinvesting) LoadQuotes(ctx context.Context) ([]*model.Quote, error) {

	logger := kitlog.With(level.Warn(l.logger), "method", "loadQuotes")

	tr := &http.Transport{
		Dial:                (&net.Dialer{Timeout: httpTimeoutConnect}).Dial,
		TLSHandshakeTimeout: httpTimeoutConnect,
		TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := &http.Client{
		Transport: tr,
		Timeout:   httpTimeoutLoad,
	}

	req, err := http.NewRequest(
		"GET",
		addressMainPage,
		nil,
	)

	// Add headers, otherwise request is rejected.
	l.setStandardHeaders(req)

	response, err := httpClient.Do(req)
	if err != nil {
		logger.Log(
			"oper", "http",
			"err", err.Error())
		return []*model.Quote{}, err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if err != nil {
		logger.Log(
			"oper", "response read",
			"err", err.Error())
		return []*model.Quote{}, err
	}

	quotes, err := l.getQuotesFromResponse(responseBody)
	if err != nil {
		logger.Log(
			"oper", "response parse",
			"err", err.Error(),
			"body", string(responseBody),
		)
		return []*model.Quote{}, err
	}

	if 0 == len(quotes) {
		logger.Log(
			"oper", "response parse",
			"warn", "check page structure",
			"body", string(responseBody),
		)
	}

	return quotes, nil
}

func (l *Forexinvesting) getQuotesFromResponse(responseBody []byte) ([]*model.Quote, error) {
	//TODO(DF): remove or move to another part of code 'allowed' instruments
	// or implement some another instruments filtering feature.
	pairsWhiteList := make(map[string]bool, 0)
	pairsWhiteList[`EURUSD`] = true
	pairsWhiteList[`GBPUSD`] = true
	pairsWhiteList[`USDJPY`] = true

	quotes := make([]*model.Quote, 0)

	partsQuote := strings.SplitAfter(string(responseBody), `<tr id="pair_`)
	if 0 == len(partsQuote) {
		return quotes, errors.New("page structure has been changed")
	}

	errs := make([]error, 6)
	var (
		tmpStr         string
		bidVal, askVal float64
	)
	for _, part := range partsQuote {

		// data-name="EUR/USD"
		tmpStr, errs[0] = stringtools.GetSubstring(part, `data-name="`, `"`)
		tmpStr = strings.ReplaceAll(tmpStr, `\`, ``)
		tmpStr = strings.ReplaceAll(tmpStr, `/`, ``)
		tmpStr = strings.ReplaceAll(tmpStr, ` `, ``)

		//TODO(DF): correct after black-white-list implemented.
		if "" == tmpStr || !pairsWhiteList[tmpStr] {
			continue
		}
		instrumentName := tmpStr

		//<td class="pid-1-bid">1.1081</td>
		//<td class="pid-1-ask">1.1082</td>
		tmpStr, errs[1] = stringtools.GetSubstring(part, `-bid">`, `<`)
		bidVal, errs[2] = strconv.ParseFloat(tmpStr, 64)

		tmpStr, errs[3] = stringtools.GetSubstring(part, `-ask">`, `<`)
		askVal, errs[4] = strconv.ParseFloat(tmpStr, 64)

		//TODO(DF): get timestamp also from HTML. Make sure it is in UTC.
		// //<td class=" pid-1-time" data-value="1566514658">18:57:38</td>
		// timestampStr, errs[5] = stringtools.GetSubstring(part, `data-value="`, `"`)

		// return in case of any error
		for _, err := range errs {
			if err != nil {
				return []*model.Quote{}, err
			}
		}

		quotes = append(quotes, &model.Quote{
			InstrumentName: instrumentName,
			QuoteValue: model.QuoteValue{
				ExchangeRate: (bidVal + askVal) / 2,
				Time:         time.Now().UTC(),
			},
		})
	}

	return quotes, nil
}

func (l *Forexinvesting) setStandardHeaders(req *http.Request) {

	req.Header.Add("Host", "www.investing.com:443")
	req.Header.Add("Accept",
		"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
	req.Header.Add("Sec-Fetch-Mode", "navigate")
	req.Header.Add("Sec-Fetch-Site", "none")
	req.Header.Add("Upgrade-Insecure-Requests", "1")
	req.Header.Add("User-Agent",
		"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36")

}
