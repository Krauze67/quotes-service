package errors

import (
	"errors"
)

//
// Service errors.
//
var (
	//
	// Common ones.
	//
	ErrInvalidRequest = errors.New("invalid request")
	ErrInternal       = errors.New("internal service error")

	//TODO(DF): add more specific errors
)
