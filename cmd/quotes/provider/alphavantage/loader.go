package alphavantage

import (
	"context"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"time"

	"github.com/buger/jsonparser"
	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Alphavantage loads quotes from https://www.alphavantage.co
//
type Alphavantage struct {
	logger kitlog.Logger
}

//
// New creates a new instance with logger initialized
//
func New(logger kitlog.Logger) *Alphavantage {
	return &Alphavantage{
		logger: logger,
	}
}

const (
	httpTimeoutLoad    = time.Duration(5) * time.Second
	httpTimeoutConnect = time.Duration(2) * time.Second

	//TODO(DF): implement to load from config in case this provider selected.
	// 48FY7ISZJWM9ROT1
	// Q6ID2SSY360QRNMO
	// V3PJ7YZI70ZCI7ZO
	// ZMYZOTL9IJ5H8SAO
	// 3RIVTZ5EDODSQKL9
	// R2JK27PQ82XCOUYN
	apiKey = "Q6ID2SSY360QRNMO"
	// e.g https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=JPY&apikey=demo
	addressFmt = "https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=%s&to_currency=%s&apikey=%s"
)

type pair struct {
	From string
	To   string
}

//TODO(DF): get instruments white list from config or wherever.
var (
	pairs = []pair{
		{"USD", "EUR"},
		//{"USD", "JPY"},
		//{"USD", "CHF"},
		// {"USD", "AUD"},
		// {"USD", "CAD"},
		// {"JPY", "CHF"},
		// {"JPY", "AUD"},
	}
)

//
// LoadQuotes implements qoutes.Loader interface
//
func (l *Alphavantage) LoadQuotes(ctx context.Context) ([]*model.Quote, error) {
	logger := kitlog.With(level.Warn(l.logger), "method", "loadQuotes")

	tr := &http.Transport{
		Dial:                (&net.Dialer{Timeout: httpTimeoutConnect}).Dial,
		TLSHandshakeTimeout: httpTimeoutConnect,
		TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := &http.Client{
		Transport: tr,
		Timeout:   httpTimeoutLoad,
	}

	quotes := make([]*model.Quote, 0, len(pairs))
	for _, pair := range pairs {

		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf(addressFmt, pair.From, pair.To, apiKey),
			nil,
		)

		response, err := httpClient.Do(req)
		if err != nil {
			logger.Log(
				"instrument", l.getInstrument(pair.From, pair.To),
				"oper", "http",
				"err", err.Error())
			continue
		}
		defer response.Body.Close()
		responseBody, err := ioutil.ReadAll(response.Body)

		if err != nil {
			logger.Log(
				"instrument", l.getInstrument(pair.From, pair.To),
				"oper", "response read",
				"err", err.Error())
			continue
		}

		quote, err := l.getQuoteFromResponse(pair.From, pair.To, responseBody)
		if err != nil {
			logger.Log(
				"instrument", l.getInstrument(pair.From, pair.To),
				"oper", "response parse",
				"err", err.Error(),
				"body", string(responseBody),
			)
			continue
		}

		quotes = append(quotes, quote)
	}

	return quotes, nil
}

func (l *Alphavantage) getQuoteFromResponse(from, to string, responseBody []byte) (*model.Quote, error) {
	// real server response:
	// {
	//     "Realtime Currency Exchange Rate": {
	//         "1. From_Currency Code": "USD",
	//         "2. From_Currency Name": "United States Dollar",
	//         "3. To_Currency Code": "JPY",
	//         "4. To_Currency Name": "Japanese Yen",
	//         "5. Exchange Rate": "106.48000000",
	//         "6. Last Refreshed": "2019-08-21 15:41:17", // ignore for a while
	//         "7. Time Zone": "UTC",
	//         "8. Bid Price": "106.48000000",
	//         "9. Ask Price": "106.48000000"
	//     }
	// }
	val, err := jsonparser.GetString(responseBody, "Realtime Currency Exchange Rate", "5. Exchange Rate")
	if err != nil {
		return nil, err
	}

	exchangeRate, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return nil, err
	}

	return &model.Quote{
		InstrumentName: l.getInstrument(from, to),
		QuoteValue: model.QuoteValue{
			ExchangeRate: exchangeRate,
			Time:         time.Now().UTC(),
		},
	}, nil
}

//
// getInstrument construct an instrument name in a proper format.
//
func (l *Alphavantage) getInstrument(from, to string) string {

	return fmt.Sprintf("%s%s", from, to)
}
