package quotes

import (
	"context"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Provider abstract quotes loader.
//
type Provider interface {
	LoadQuotes(ctx context.Context) ([]*model.Quote, error)
}
