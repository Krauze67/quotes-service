package collector

import (
	"context"
	"fmt"
	"sync"
	"time"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes"
	svclog "gitlab.com/Krauze67/quotes-service/cmd/quotes/log"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/repository"
)

const (
	logSubsys = "loader"
	logkey    = "msg"

	loadPeriod = time.Duration(15) * time.Second
)

type collector struct {
	ctx       context.Context
	provider  quotes.Provider
	reschan   chan error
	logger    kitlog.Logger
	interrupt bool
}

//
// Run starts a go-routine of quotes loading
//
func Run(ctx context.Context, provider quotes.Provider, resulterror chan error) {
	c := &collector{
		ctx:       ctx,
		provider:  provider,
		reschan:   resulterror,
		logger:    kitlog.With(svclog.Logger, "subsys", logSubsys),
		interrupt: false,
	}
	go func() {

		var wg sync.WaitGroup
		wg.Add(1)
		go c.loaderLoop(&wg)

		level.Info(c.logger).Log(logkey, "started")
		wg.Wait()
		level.Info(c.logger).Log(logkey, "stopped")
	}()
}

func (c *collector) loaderLoop(wg *sync.WaitGroup) {

	var err error

	for !c.interrupt {

		quotes, err := c.provider.LoadQuotes(c.ctx)
		if err == nil {
			err = repository.Repository.SaveQuotes(c.ctx, quotes)
		}
		level.Debug(c.logger).Log(logkey, fmt.Sprintf("%d quotes loaded", len(quotes)))

		if err != nil {
			c.interrupt = true
			continue
		}

		select {
		case <-c.ctx.Done():
			c.interrupt = true
		case <-time.After(loadPeriod):
			//just wait a bit between quotes loading
		}
	}
	c.reschan <- err
	wg.Done()
}
