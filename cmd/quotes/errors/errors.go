package errors

import (
	"errors"
)

//
// Service errors.
//
var (
	//
	// Common ones.
	//
	ErrInvalidRequest = errors.New("invalid request")
	ErrInternal       = errors.New("internal service error")

	//TODO(DF): add more specific errors, at least(for HTTP 404)
	ErrInstrumentNotFound = errors.New("no such instrument")
)
