package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	configRequiredParamNameDSN = "QUOTESSVC_DB_DSN"
)

//
// config. try to load with empty DB_DSN (required).
// Should fail to load.
//
func TestConfigLoadRequiredDatabaseDSN(t *testing.T) {
	paramDSN := os.Getenv(configRequiredParamNameDSN)
	defer os.Setenv(configRequiredParamNameDSN, paramDSN)

	os.Setenv(configRequiredParamNameDSN, "")

	err := Parameters.Load()

	assert.Error(t, err)
	assert.Equal(t, Parameters.DatabaseDSN, "")
}

//
// config. try to load with filled DB_DSN (required).
// Should succeed.
//
func TestConfigLoad(t *testing.T) {
	paramDSN := os.Getenv(configRequiredParamNameDSN)
	defer os.Setenv(configRequiredParamNameDSN, paramDSN)

	testVal := "SomeNotEmptyTestValue"
	os.Setenv(configRequiredParamNameDSN, testVal)

	err := Parameters.Load()

	assert.Nil(t, err)
	assert.Equal(t, Parameters.DatabaseDSN, testVal)
}
