package service

import (
	"context"
	"fmt"
	"time"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/errors"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// service implements the service.
//
type service struct {
	repository quotes.Repository
	logger     kitlog.Logger
}

//
// New creates and returns a new service instance.
//
func New(repo quotes.Repository, log kitlog.Logger) quotes.Service {
	return &service{
		repository: repo,
		logger:     log,
	}
}

//
// GetStatus returns a current summary: exchange rate and average rates by current day, week and month.
//
func (s *service) GetStatus(ctx context.Context, instrument string) (model.CurrentStatus, error) {
	logger := kitlog.With(s.logger, "method", "GetStatus")

	status, err := s.repository.GetStatus(ctx, instrument)
	if err != nil {
		level.Error(logger).Log("err", err)
	}
	return status, err
}

//
// GetHistoryValue returns an instrument rate nearest to a timestamp given.
//
func (s *service) GetHistoryValue(ctx context.Context, instrument string, time time.Time) (model.Quote, error) {
	logger := kitlog.With(s.logger, "method", "GetHistoryValue")

	quote, err := s.repository.GetHistoryValue(ctx, instrument, time)
	if err != nil {
		level.Error(logger).Log("err", err)
	}
	return quote, err
}

//
// GetHistoryValue returns an instrument rates aggregated by periods.
//
func (s *service) GetHistorySeries(
	ctx context.Context,
	instrument string,
	aggregationType string,
	begin time.Time,
	end time.Time,
) ([]model.QuoteValue, error) {
	logger := kitlog.With(s.logger, "method", "GetHistorySeries")

	minutes, err := s.convertAggregationToMinutes(aggregationType)
	if err != nil {
		level.Error(logger).Log("err", err)
		return []model.QuoteValue{}, err
	}
	if minutes < 0 {
		return []model.QuoteValue{}, errors.ErrInvalidRequest
	}

	quotes, err := s.repository.GetHistorySeries(
		ctx,
		instrument,
		minutes,
		begin,
		end,
	)
	if err != nil {
		level.Error(logger).Log("err", err)
		return []model.QuoteValue{}, err
	}

	return quotes, err
}

func (s *service) convertAggregationToMinutes(aggregationType string) (int64, error) {
	var (
		n      int64
		period string
	)
	if _, err := fmt.Sscanf(aggregationType, "%d%s", &n, &period); err != nil {
		return 0, err
	}

	switch period {
	case "m":
		// default minimum, do nothing
	case "h":
		n *= 60
	case "d":
		n *= 24 * 60
	default:
		return -1, nil
	}

	return n, nil
}
