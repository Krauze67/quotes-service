package http

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	quoteerrors "gitlab.com/Krauze67/quotes-service/cmd/quotes/errors"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/transport"
)

const (
	paramInstrument  = "instrument"
	paramTimestamp   = "ts"
	paramPeriodBegin = "begin"
	paramPeriodEnd   = "end"
	paramJoinPeriod  = "period"
)

var (
	//
	// ErrBadRouting to return in case no according endpoint found.
	//
	ErrBadRouting = errors.New("bad routing")
)

//
// NewService wires Go kit endpoints to the HTTP transport.
//
func NewService(
	svcEndpoints transport.Endpoints,
	logger log.Logger,
) http.Handler {
	// set-up router and initialize http endpoints

	r := mux.NewRouter()
	r.Use(commonMiddleware)

	options := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(encodeError),
	}

	// HTTP Get - /summary/{instrument}
	r.Methods("GET").Path("/summary/{" + paramInstrument + ":.*}").
		Handler(kithttp.NewServer(
			svcEndpoints.GetStatus,
			decodeGetStatusRequest,
			encodeResponse,
			options...,
		))

	// HTTP Get - /history/value/{instrument}?{ts}
	r.Methods("GET").Path("/history/value/{" + paramInstrument + ":.*}").
		Handler(kithttp.NewServer(
			svcEndpoints.GetHistoryValue,
			decodeHistoryValueRequest,
			encodeResponse,
			options...,
		))

	// HTTP Get - /history/series/{instrument}?{period}{begin}{end}
	r.Methods("GET").Path("/history/series/{" + paramInstrument + ":.*}").
		Handler(kithttp.NewServer(
			svcEndpoints.GetHistorySeries,
			decodeHistorySeriesRequest,
			encodeResponse,
			options...,
		))

	return r
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func decodeGetStatusRequest(_ context.Context, r *http.Request) (request interface{}, err error) {

	instrument := strings.ToUpper(mux.Vars(r)[paramInstrument])
	return transport.StatusRequest{Instrument: instrument}, nil
}

func decodeHistoryValueRequest(_ context.Context, r *http.Request) (request interface{}, err error) {

	instrument := strings.ToUpper(mux.Vars(r)[paramInstrument])

	tsStr := r.URL.Query().Get(paramTimestamp)
	tsVal, _ := strconv.ParseInt(tsStr, 10, 64)
	if tsVal <= 0 {
		return transport.HistoryValueRequest{}, quoteerrors.ErrInvalidRequest
	}

	return transport.HistoryValueRequest{
		Instrument: instrument,
		Time:       time.Unix(tsVal, 0),
	}, nil
}

func decodeHistorySeriesRequest(_ context.Context, r *http.Request) (request interface{}, err error) {

	instrument := strings.ToUpper(mux.Vars(r)[paramInstrument])
	joinPeriod := r.URL.Query().Get(paramJoinPeriod)
	if "" == joinPeriod {
		return transport.HistorySeriesRequest{}, quoteerrors.ErrInvalidRequest
	}

	tsStr := r.URL.Query().Get(paramPeriodBegin)
	begin, _ := strconv.ParseInt(tsStr, 10, 64)

	tsStr = r.URL.Query().Get(paramPeriodEnd)
	end, _ := strconv.ParseInt(tsStr, 10, 64)

	if begin <= 0 || end <= 0 || begin >= end+59 { // accept at least 1 minute period
		return transport.HistorySeriesRequest{}, quoteerrors.ErrInvalidRequest
	}

	return transport.HistorySeriesRequest{
		Instrument:      instrument,
		AggregationType: joinPeriod,
		PeriodBegin:     time.Unix(begin, 0),
		PeriodEnd:       time.Unix(end, 0),
	}, nil
}

type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	return kithttp.EncodeJSONResponse(ctx, w, response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))

	json.NewEncoder(w).Encode(transport.ErrorResponse{
		Error: err.Error(),
	})
}

func codeFrom(err error) int {
	//TODO(DF): add errors processing here if they should be represented by
	// some specific HTTP status
	switch err {
	case quoteerrors.ErrInvalidRequest:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
