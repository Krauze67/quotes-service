package transport

import (
	"time"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// ErrorResponse to return an error.
//
type ErrorResponse struct {
	Error string `json:"error,omitempty"`
}

//
// StatusRequest holds information to get current summary by instrument.
//
type StatusRequest struct {
	Instrument string `json:"instrument,omitempty"`
}

//
// StatusResponse holds information about instrument.
//
type StatusResponse struct {
	Instrument string    `json:"instrument,omitempty"`
	Time       time.Time `json:"time,omitempty"`
	model.CurrentStatus
}

//
// HistoryValueRequest holds all parameters required to request an instrument historical
// exchange rate(nearest to a time given).
//
type HistoryValueRequest struct {
	Instrument string
	Time       time.Time
}

//
// HistoryValueResponse contains historical quote value.
//
type HistoryValueResponse struct {
	model.Quote
}

//
// HistorySeriesRequest holds all parameters required to request an instrument historical
// exchange rate series: aggregation period, requested period begin/enf.
//
type HistorySeriesRequest struct {
	Instrument      string
	AggregationType string
	PeriodBegin     time.Time
	PeriodEnd       time.Time
}

//
// HistorySeriesResponse quotes series to return.
//
type HistorySeriesResponse struct {
	Instrument  string             `json:"instrument,omitempty"`
	PeriodBegin time.Time          `json:"begin,omitempty"`
	PeriodEnd   time.Time          `json:"end,omitempty"`
	Quotes      []model.QuoteValue `json:"values,omitempty"`
}
