package quotes

import (
	"context"
	"time"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/model"
)

//
// Service describes the service functions.
//
type Service interface {
	GetStatus(ctx context.Context, instrument string) (model.CurrentStatus, error)
	GetHistoryValue(ctx context.Context, instrument string, time time.Time) (model.Quote, error)
	GetHistorySeries(
		ctx context.Context,
		instrument string,
		aggregationType string,
		begin time.Time,
		end time.Time,
	) ([]model.QuoteValue, error)
}
