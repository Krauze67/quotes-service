package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/kit/log/level"
	_ "github.com/lib/pq"

	"gitlab.com/Krauze67/quotes-service/cmd/quotes/collector"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/config"
	svclog "gitlab.com/Krauze67/quotes-service/cmd/quotes/log"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/provider/forexinvesting"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/repository"
	svcrepo "gitlab.com/Krauze67/quotes-service/cmd/quotes/repository"
	quotessvc "gitlab.com/Krauze67/quotes-service/cmd/quotes/service"
	"gitlab.com/Krauze67/quotes-service/cmd/quotes/transport"
	httptransport "gitlab.com/Krauze67/quotes-service/cmd/quotes/transport/http"
)

const (
	logkey = "main"
)

func main() {

	var err error
	// Config
	if err = config.Parameters.Load(); err != nil {
		panic(err)
	}

	// Logger
	svclog.InitLogger()
	level.Info(svclog.Logger).Log(logkey, "service started")
	defer level.Info(svclog.Logger).Log(logkey, "service ended")

	// Repository
	level.Info(svclog.Logger).Log(logkey, "creating a DB repository")
	err = svcrepo.InitRepository(svclog.Logger)
	if err != nil {
		level.Error(svclog.Logger).Log("exit", err)
		os.Exit(-1)
	}
	level.Info(svclog.Logger).Log(logkey, "DB repository has been created")

	// Quotes loader
	errs := make(chan error)
	defer close(errs)
	ctx, stopQuotesLoader := context.WithCancel(context.Background())
	//too big limitations for free accounts: collector.Run(ctx, alphavantage.New(svclog.Logger), errs)
	collector.Run(ctx, forexinvesting.New(svclog.Logger), errs)

	// HTTP handler
	var h http.Handler
	{
		level.Info(svclog.Logger).Log(logkey, "HTTP transport init")
		endpoints := transport.MakeEndpoints(quotessvc.New(repository.Repository, svclog.Logger))
		h = httptransport.NewService(endpoints, svclog.Logger)
		level.Info(svclog.Logger).Log(logkey, "HTTP transport init done")
	}

	// SIGINT, SIGTERM handling
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	// start listening
	go func() {
		level.Info(svclog.Logger).Log("transport", "HTTP", "addr", config.Parameters.ListenAddress)
		server := &http.Server{
			Addr:        config.Parameters.ListenAddress,
			Handler:     h,
			ReadTimeout: time.Duration(config.Parameters.HTTPTimeoutSec) * time.Second,
		}
		errs <- server.ListenAndServe()
	}()

	err = <-errs

	stopQuotesLoader()

	level.Error(svclog.Logger).Log("exit", err)
}
