// +build functional

package quotes

import (
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	URLSummary = URLBase + "/summary"
)

func TestGetSummary(t *testing.T) {

	httpRequest, err := http.NewRequest(
		http.MethodGet,
		URLSummary+"/"+paramInstrument,
		nil,
	)
	assert.Nil(t, err)

	HTTPClient := &http.Client{
		Timeout: time.Second * 5,
	}

	httpResponse, err := HTTPClient.Do(httpRequest)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, httpResponse.StatusCode)

	defer func() {
		assert.Nil(t, httpResponse.Body.Close())
	}()

	_, err = ioutil.ReadAll(httpResponse.Body)
	assert.Nil(t, err)
}
