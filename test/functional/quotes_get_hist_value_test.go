// +build functional

package quotes

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	//http://127.0.0.1:8080/history/value/EURUSD?ts=1566577274

	URLHistoryValue = URLBase + "/history/value"

	testvalTs = 1566577274
)

func TestGetHistoryValue(t *testing.T) {

	url := fmt.Sprintf(
		"%s/%s?%s=%d",
		URLHistoryValue,
		paramInstrument,
		paramTimestamp,
		testvalTs,
	)

	httpRequest, err := http.NewRequest(
		http.MethodGet,
		url,
		nil,
	)
	assert.Nil(t, err)

	HTTPClient := &http.Client{
		Timeout: time.Second * 5,
	}

	httpResponse, err := HTTPClient.Do(httpRequest)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, httpResponse.StatusCode)

	defer func() {
		assert.Nil(t, httpResponse.Body.Close())
	}()

	_, err = ioutil.ReadAll(httpResponse.Body)
	assert.Nil(t, err)
}
