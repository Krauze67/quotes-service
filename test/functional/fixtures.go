package quotes

//
// URLs.
//
const (
	URLBase = "http://quotes-service:8080"
)

//
// Requests parameters.
//
const (
	paramInstrument = "EURUSD"
	paramTimestamp  = "ts"
)
