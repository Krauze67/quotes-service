// +build functional

package quotes

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	//http://127.0.0.1:8080/history/series/EURUSD?period=30m&begin=1166577274&end=1866586507

	URLAggregatedValue = URLBase + "/history/series"

	paramPeriod = "period"
	paramBegin  = "begin"
	paramEnd    = "end"

	testvalPeriod = "5m"
	testvalBegin  = 1166577274
	testvalEnd    = 1866586507
)

func TestGetAggregated(t *testing.T) {

	url := fmt.Sprintf(
		"%s/%s?%s=%s&%s=%d&%s=%d",
		URLAggregatedValue,
		paramInstrument,
		paramPeriod, testvalPeriod,
		paramBegin, testvalBegin,
		paramEnd, testvalEnd,
	)

	httpRequest, err := http.NewRequest(
		http.MethodGet,
		url,
		nil,
	)
	assert.Nil(t, err)

	HTTPClient := &http.Client{
		Timeout: time.Second * 5,
	}

	httpResponse, err := HTTPClient.Do(httpRequest)
	assert.Nil(t, err)
	assert.NotNil(t, httpResponse)
	assert.Equal(t, http.StatusOK, httpResponse.StatusCode)

	defer func() {
		assert.Nil(t, httpResponse.Body.Close())
	}()

	_, err = ioutil.ReadAll(httpResponse.Body)
	assert.Nil(t, err)
}
