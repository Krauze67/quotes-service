# Quotes Service

# Topics
* [Overview](#overview)
* [Providers](#providers)
* [Parameters](#parameters)
* [Build](#build)
* [Endpoints](#endpoints) 
  * [GET /summary](#get-summary)
  * [GET /history/value](#get-historyvalue)
  * [GET history/series](#get-historyseries)
* [Notes](#notes)

# Overview
A `Quotes Service` is a test code to implement some kind of financial/trading information cache RESTful service using `go-kit`. 
It loads financial info from one of [providers](#providers) and saves it in Postgres DB.
Allows to get next information on data collected:
1. Current instrument status: `Last price` and average prices for current day, current week and current month. See [GET /summary](#get-summary) for details.
2. Historical instrument price: an instrument price, nearest to a time given. See [GET /history/value](#get-historyvalue) for details.
3. Aggregated info for period: average price of an instrument by some interval (`1m`, `5m`, `1h`, `4h`, `1d` etc) for selected time period since `begin` to `end`. See [GET history/series](#get-historyseries) for details.

# Providers
Currently implemented work with 2 financial info providers(`cmd/providers`): 
1. [Alpha Vantage](https://www.alphavantage.co) - free, history data/realtime, JSON reply, big limitations on Free plan.
2. [Forex Investing](https://www.investing.com/currencies/single-currency-crosses) - realtime, single HTML page which contains a lot of instruments. 
For test purposes small amount of data collected could be used. Look `build/postgresql/service_fixtures.sql` or just run a docker cluster(see [Build & Install](#build-&-install) below) and this data will be available to test an API provided.

# Parameters
`Quotes Service` uses next listed environment variables to load its configuration:

| Env. variable         |         Content              | Default value
|-----------------------|----------------------------|-----------------------------------
| QUOTESSVC_LOG_LEVEL      | Log level. Allowed values are: `DEBUG`, `INFO`, `WARNING`, `ERROR`          | `WARNING`
| QUOTESSVC_LISTEN_ADDRESS | An `[address]:port` to listen.          | `:8080`
| QUOTESSVC_DB_DSN         | A full database DSN to connect to PostgreSQL DB layer. e.g. `postgres://postgres:postgres@wallet-postgres:5432/wallet?sslmode=disable` |


# Build
The project uses `go mod` feature to vendor packages, so use *Go* of version *1.11* or higher and set **GO111MODULE** environment variable to `on`.
```
export GO111MODULE=on
```
You could build the project as usual, by run `go get && go build` inside `./cmd` folder.
There is recommended method to build and run the service by `Makefile` in a sources root.
run `make` with one of below listed `Target` to execute an`Action`:

**Make targets**

| Target            |         Action                             |
|-------------------|--------------------------------------------|
| go_test_unit      | run unit tests. | 
| build             | run unit tests and build the service binary file.              |
| docker_image      | build a docker image using *Go 1.12* image and create a new image with service binary. | 
| cluster_start     | build and start a cluster of all required docker containers (`PostgreSQL` and `Quotes Service`).|
| cluster_stop      | stop the cluster and remove according docker containers. |  
| cluster_restart   | cluster_stop, then cluster_start. | 
| cluster_run_tests | run functional tests on a running cluster. | 
| cluster_test      | cluster build, run, run functional tests. | 

**Most common scenarios:**
- Build binary: Run `make build` to get a `quotes-service` binary in a project root.
- Run & test: Run `make cluster_start` to get a running containers with service listening a local port `8080`.

**NOTE** you could manage cluster parameters by change according valules inside `build/quotes-service.env` file.


# Endpoints
## GET /summary
Used to get an summary information by instrument: current rate and average rates for current day, week and month.
 
### Parameters:
`Instrument` - an instrument name. Placed as URL suffix: `/history/value/{value}`

**Request info**
```
HTTP Request method    GET
Request URL            http://127.0.0.1:8080/summary/EURUSD
```
Returns a summary info e.g.:

**Response**
```json
{
    "instrument": "EURUSD",
    "time": "2019-08-24T16:53:45.892447001Z",
    "current": 1.1142,
    "today_average": 1.1142,
    "week_average": 1.1137767806267806,
    "month_average": 1.1137767806267806
}
```

## GET /history/value
Used to get an information about historical value of instrument rate, nearest to a timestamp given. 
### Parameters:
`Instrument` - an instrument name. Placed as URL suffix: `/history/value/{value}`
`ts`- a timestamp to get an instrument rate. It is a URL query value `...?ts={value}`

**Request info**
```
HTTP Request method    GET
Request URL            http://127.0.0.1:8080/history/value/EURUSD?ts=1566577274
```
Returns a historical value for timestamp e.g.:

**Response**
```json
{
    "name": "EURUSD",
    "exchangerate": 1.1129,
    "time": "2019-08-23T16:21:11.285695Z"
}
```

## GET /history/series
Used to get an aggregated history information of instrument using arbitrary aggregation time interval and history period.
 
### Parameters:
`Instrument` - an instrument name. Placed as URL suffix: `/history/value/{value}`
Followinf parameters are all URL query values:
`period` - aggregation period as integer number of periods. `N{period abbreviation}`. 
Supports `m`(minute), `h`(hours and `d`(days) periods. So half of an hour period is `30m` etc.
`begin`,`end`- a timestamps of history period begin and end respectively

**Request info**
```
HTTP Request method    GET
Request URL            http://127.0.0.1:8080/history/series/EURUSD?period=15m&begin=1166577274&end=1866586507
```
Returns an instrument name, time interval bounds and historical values for each `period` between `begin` and `end`:

**Response**
```json
{
    "instrument": "EURUSD",
    "begin": "2006-12-20T01:14:34Z",
    "end": "2029-02-24T00:15:07Z",
    "values": [
        {
            "exchangerate": 1.113938679245283,
            "time": "2019-08-23T15:30:00Z"
        },
        {
            "exchangerate": 1.1132768181818182,
            "time": "2019-08-23T16:00:00Z"
        },
        {
            "exchangerate": 1.1137909836065574,
            "time": "2019-08-23T16:30:00Z"
        },
        {
            "exchangerate": 1.1140935064935065,
            "time": "2019-08-23T17:00:00Z"
        },
        {
            "exchangerate": 1.1142,
            "time": "2019-08-24T16:30:00Z"
        }
    ]
}
```

# Notes

* reimplement all to use a trading-style values: `Bid`, `Ask` instead of `Exchange rate`. Including aggregated periods values: `Open`, `High`, `Low`, `Close`.
* It was a bad idea to join loader and service in a single service. Need to split them to separate micro services. 

**Loader:**

* need to implement some HTTP errors handling to continue work in case timeout or some network error occurs.

**Quotes Service:**

* need to add `instruments cache` inside a repository to increase a quotes save performance.
* need to add correct processing of SQL `Not found` errors to correctly return HTTP 404 status.
* need to add some translator between internal errors (DB, business-logic, etc) and a final unmarshaller to hide internals from user.
* possibly it is unnecessary to return Instrument info back to user.
* possibly implement specific Unmarshaller for returned time values to return them as timestamp values.


