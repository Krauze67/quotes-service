
-- CREATE DATABASE quotesdb WITH ENCODING='UTF8';

GRANT ALL PRIVILEGES ON DATABASE quotesdb TO postgres;

CREATE TABLE IF NOT EXISTS instruments (
   id serial PRIMARY KEY,
   name VARCHAR (100) UNIQUE NOT NULL CHECK(length(name)>0)
);


CREATE TABLE IF NOT EXISTS quotes (
   id serial PRIMARY KEY,

   instrument_id INTEGER NOT NULL,
   got_at TIMESTAMP NOT NULL CHECK(got_at <= now() AND isfinite(got_at)),
   exchange_rate NUMERIC(40, 20) NOT NULL CHECK (exchange_rate > 0),

   CONSTRAINT quotes_instrument_id_fkey FOREIGN KEY (instrument_id)
      REFERENCES instruments(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


